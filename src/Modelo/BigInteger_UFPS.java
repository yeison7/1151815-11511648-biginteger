/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author Yeison
 */
public class BigInteger_UFPS {

    /**
     * Numero="23456" miNUmero={2,3,4,5,6};
     */
    private int miNumero[];

    public BigInteger_UFPS() {
    }

    public BigInteger_UFPS(String miNumero) {

        String[] parts = miNumero.split("");
        this.miNumero = new int[parts.length];
        for (int i = 0; i < parts.length; i++) {
            this.miNumero[i] = Integer.valueOf(parts[i]);
        }

    }

    public int[] getMiNumero() {
        return miNumero;
    }

    public BigInteger_UFPS sumar(BigInteger_UFPS dos) {

        int a = Math.abs(miNumero.length - dos.getMiNumero().length);
        if (miNumero.length > dos.getMiNumero().length) {
            for (int i = 0; i < a; i++) {

            }
        }
        return dos;
    }

    /**
     * Mutiplica dos enteros BigInteger
     *
     * @param dos
     * @return
     */
    public BigInteger_UFPS multiply(BigInteger_UFPS dos) {
        String x = this.toString();
        String y = dos.toString();
        String aux = mult_string(x, y);
        BigInteger_UFPS resul = new BigInteger_UFPS(aux);
        return resul;
    }

    /**
     * Retorna la representación entera del BigInteger_UFPS
     *
     * @return un entero
     */
    public int intValue() {

        String res = "";

        for (int i = 0; i < miNumero.length; i++) {
            res += Integer.toString(miNumero[i]);
            if (res.length() > 9) {
                break;
            }

        }
        int resul = Integer.valueOf(res);

        return resul;
    }

    public static String mult_string(String x, String y) {
        int res[] = new int[x.length() + y.length()];

        int i_d1 = 0;
        int i_d2 = 0;

        for (int i = x.length() - 1; i >= 0; i--) {
            int carry = 0;
            int d1 = x.charAt(i) - '0';
            i_d2 = 0;
            for (int j = y.length() - 1; j >= 0; j--) {
                int d2 = y.charAt(j) - '0';
                int sum = d1 * d2 + res[i_d1 + i_d2] + carry;
                carry = sum / 10;

                res[i_d1 + i_d2] = sum % 10;
                i_d2++;
            }

            if (carry > 0) {
                res[i_d1 + i_d2] += carry;
            }
            
            i_d1++;
        }

        int i = res.length - 1;
        while (i >= 0 && res[i] == 0) {
            i--;
        }
        String s = "";

        while (i >= 0) {
            s += (res[i--]);
        }
        return s;
    }

    @Override
    public String toString() {
        String nro = "";
        for (int i = 0; i < miNumero.length; i++) {
            nro = nro + Integer.toString(miNumero[i]);
        }

        return nro;
    }
}
